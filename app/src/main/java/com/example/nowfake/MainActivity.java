package com.example.nowfake;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity implements lichsu.OnFragmentInteractionListener,dangden.OnFragmentInteractionListener,DonNhap.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabLayout tabLayout=(TabLayout)findViewById(R.id.tablelayout);
        tabLayout.addTab(tabLayout.newTab().setText("dang den"));
        tabLayout.addTab(tabLayout.newTab().setText("Lich su"));
        tabLayout.addTab(tabLayout.newTab().setText("don nhap"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager=(ViewPager) findViewById(R.id.pager);
        final PagerApapter adater =new PagerApapter(getSupportFragmentManager(),tabLayout.getTabCount());
        viewPager.setAdapter(adater);
        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
});
}

    public void onFragmentInteraction(Uri uri){
        //you can leave it empty
    }
}